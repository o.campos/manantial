const ParseVisit = require("../ParseVisit");

const line =
  "20190326000000001416910O213029042808100181033CH1903105      82832130290443A0001720286005520005669500000002000000000200000006000000870307000000100000000025000000001000";
const parseVisit = new ParseVisit(line);

test("IdUbicacion", () => {
  const from = 8;
  const to = 23;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getIdUbicacion();
  expect(result).toEqual(expectedResult);
});

test("NumeroDeOrden", () => {
  const from = 64;
  const to = 74;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getNumeroDeOrden();
  expect(result).toEqual(expectedResult);
});
test("Selector Orden", () => {
  const from = 74;
  const to = 75;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getSelectorOrden();
  expect(result).toEqual(expectedResult);
});
test("Tipo de Orden", () => {
  const from = 23;
  const to = 24;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getTipoDeOrden();
  expect(result).toEqual(expectedResult);
});
test("Id de Articulo", () => {
  const from = 90;
  const to = 98;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getIdDeArticulo();
  expect(result).toEqual(expectedResult);
});
test("Tamaño SKU Total 1", () => {
  const from = 98;
  const to = 106;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getTamanoSkuTotal1();
  expect(result).toEqual(expectedResult);
});
test("Tamaño SKU Total 2", () => {
  const from = 106;
  const to = 117;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getTamanoSkuTotal2();
  expect(result).toEqual(expectedResult);
});
test("Tamaño SKU Total 3", () => {
  const from = 117;
  const to = 128;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getTamanoSkuTotal3();
  expect(result).toEqual(expectedResult);
});
test("OC", () => {
  const from = 45;
  const to = 56;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getOC();
  expect(result).toEqual(expectedResult);
});
test("FechaVenc", () => {
  const from = 56;
  const to = 60;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getFechaVenc();
  expect(result).toEqual(expectedResult);
});

test("FechaSesion", () => {
  const from = 0;
  const to = 8;
  const expectedResult = line.slice(from, to);

  const result = parseVisit.getFechaSesion();
  expect(result).toEqual(expectedResult);
});
