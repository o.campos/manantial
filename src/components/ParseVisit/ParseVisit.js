class ParseVisit {
  constructor(line) {
    this.line = line;
  }

  getIdUbicacion() {
    return this.line.slice(8, 23);
  }
  getNumeroDeOrden() {
    return this.line.slice(64, 74);
  }
  getSelectorOrden() {
    return this.line.slice(74, 75);
  }
  getTipoDeOrden() {
    return this.line.slice(23, 24);
  }
  getIdDeArticulo() {
    return this.line.slice(90, 98);
  }
  getTamanoSkuTotal1() {
    return this.line.slice(98, 106);
  }
  getTamanoSkuTotal2() {
    return this.line.slice(106, 117);
  }
  getTamanoSkuTotal3() {
    return this.line.slice(117, 128);
  }
  getOC() {
    return this.line.slice(45, 56);
  }
  getFechaVenc() {
    return this.line.slice(56, 60);
  }
  getFechaSesion() {
    return this.line.slice(0, 8);
  }
}

module.exports = ParseVisit;
