class Visit {
  constructor() {

  }

  setIdUbicacion(value) {
    this.idUbicacion = parseInt(value).toString();
  }
  setNumeroDeOrden(value) {
    this.numeroDeOrden = value;
  }
  setSelectorOrden(value) {
    this.selectorOrden = value;
  }
  setTipoDeOrden(value) {
    this.tipoDeOrden = value;
  }
  setIdDeArticulo(value) {
    this.idDeArticulo = parseInt(value);
  }
  setTamanoSkuTotal1(value) {
    this.tamanoSkuTotal1 = parseInt(value);
  }
  setTamanoSkuTotal2(value) {
    this.tamanoSkuTotal2 = parseFloat(value) / 1000;
  }
  setTamanoSkuTotal3(value) {
    this.tamanoSkuTotal3 = parseInt(value);
  }
  setOC(value) {
    this.oc = value;
  }
  setFechaVenc(value) {
    this.fechaVenc = value;
  }
  setFechaSesion(value) {
    this.fechaSesion = value;
  }
  setDireccion(value) {
    this.direccion = value;
  }

  setGeolocation(lat, long) {
    this.latitude = lat;
    this.longitude = long;
  }

  setRazonSocial(value){
    if(/\\/g.test(value)){
      this.razonSocial = value.replace(/\\/g, '');
    }else{
      this.razonSocial = value;
    }
  }

  setTelefono(value){
    this.telefono = value;
  }

  setEmail(value){
    this.email = value;
  }

  setNombre(value){
    this.nombre = value;
  }

  setTiempoServicio(value){
    this.tiempoServicio = value;
  }

  setVentanaHoraria(start, end){
    this.inicioVentana = start;
    this.finVentana = end;
  }

  setProducto(product){
    this.producto = product;
  }

}

module.exports = Visit;
