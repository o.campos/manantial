const {sendEmail} = require('../services/email/email');

const filterFilesByName = (arr, query) => {
  return arr.filter((el) =>
    el.name.toLowerCase().indexOf(query.toLowerCase()) > -1
  );
}

const filterFiles = (arr, query) => {
  return arr.filter((el) =>
    el.toLowerCase().indexOf(query.toLowerCase()) > -1
  );
}

const getProducts = () => {
  return products = [
    { id: 870310, label: 'MANANTIAL SG VAS250X32', weight: 0.017 },
    { id: 870311, label: 'MANANTIAL SG VAS250X48', weight: 0.02 },
    { id: 9780, label: 'DISPENSADOR PEDESTAL BOT. AF', weight: 0.111111111111111 },
    { id: 56885, label: 'MODULO PLASTICO VENTA 4 BOT.', weight: 1 },
    { id: 870313,
      label: 'MANANTIAL SG VAS120X72-JUMBO',
      weight: 0.020833333333333 },
    { id: 56705,
      label: 'CAJA ENTEL VASOS 2000 UN 200CC',
      weight: 0.05 },
    { id: 9781,
      label: 'DISPENSADOR PEDESTAL RED AF',
      weight: 0.111111111111111 },
    { id: 9785, label: 'DISPENSADOR COOLER', weight: 0.25 },
    { id: 9786, label: 'CENTRO HIDRATACION', weight: 1 },
    { id: 9787, label: 'VITRINA VERTICAL-COOLER', weight: 1 },
    { id: 870308,
      label: 'MANANTIAL SG BOTELLON 12LT',
      weight: 0.016666666666667 },
    { id: 870307,
      label: 'MANANTIAL SG BOTELLON 20LT',
      weight: 0.025 },
    { id: 56620,
      label: 'ENVASE BOTELLON 12 LTS',
      weight: 0.016666666666667 },
    { id: 56621, label: 'ENVASE BOTELLON 20 LTS', weight: 0.025 },
    { id: 870312,
      label: 'MANANTIAL SG VAS120X72',
      weight: 0.016666666666667 },
    { id: 56582,
      label: 'MANILLAS PARA BOTELLON IMPORTADA',
      weight: 0.00025 },
    { id: 269044, label: 'PORTA VASOS', weight: 0.003472222222222 },
    { id: 269048,
      label: 'CAJA VASOS PLASTICOS 90 C.C. (300 UNID.)',
      weight: 0.01 },
    { id: 269049,
      label: 'CAJA CONO DE PAPEL 200 UNIDADES',
      weight: 0.002222222222222 },
    { id: 269051,
      label: 'CAJA VASOS TERMICOS 200 C.C. (150 UNID.)',
      weight: 0.01 },
    { id: 269066,
      label: 'SOPORTE DE CERAMICA SOBREMESA',
      weight: 0.02 },
    { id: 269064, label: 'SOPORTE BOTELLON', weight: 0.01 },
    { id: 56700, label: 'SERVIFACIL', weight: 0.003472222222222 },
    { id: 56696,
      label: 'CAJA VASOS PLAST.200CC(2000UN)',
      weight: 0.05 },
    { id: 56695,
      label: 'CAJA VASOS PLAST.200CC(300 UN)',
      weight: 0.01 },
    { id: 269060, label: 'RACK 3 BOTELLONES', weight: 0.025 },
    { id: 269047,
      label: 'BOMBA MANUAL CON MANILLA',
      weight: 0.003472222222222 },
    { id: 269050,
      label: 'CAJA CONO DE PAPEL 5000 UNIDADES',
      weight: 0.055555555555556 },
    { id: 269061,
      label: 'RACK 4 BOTELLONES',
      weight: 0.033333333333333 },
    { id: 5706, label: 'RACK METAL 6 BOTELLONES', weight: 0.05 },
    { id: 999760, label: 'INCORPORACION', weight: 0.025 },
    { id: 58586,
      label: 'MUG TERMO AINOX BEIGE',
      weight: 0.000925925925926 },
    { id: 57804,
      label: 'CAJA VASOS POLI PAPEL 300 U',
      weight: 0.008333333333333 },
    { id: 58089,
      label: 'CARAMAYOLA POP AZUL 500CC',
      weight: 0.000833333333333 },
    { id: 58092,
      label: 'MUG INOXI BA 400CC',
      weight: 0.000925925925926 },
    { id: 58090,
      label: 'CARAMAYOLA POP GRIS 500CC',
      weight: 0.000833333333333 },
    { id: 57811, label: 'CAJAVASPOL6.5/2000U', weight: 0.05 },
    { id: 870797, label: 'MANANTIAL SG VAS250X20', weight: 0.01 },
    { id: 58081,
      label: 'SERVIFACIL CON VALVULA',
      weight: 0.003472222222222 },
    { id: 58358,
      label: 'CARAMAYOLA POP ROSADA 500 CC',
      weight: 0.000833333333333 },
    { id: 58474, label: 'LONCHERAKIDS', weight: 0.003333333333333 },
    { id: 58776,
      label: 'PACKLONCHERA 2 CJAVAS AGUA 20U',
      weight: 0.023333333333333 },
    { id: 58700,
      label: 'CARAMAYOLA NIÑO AZUL',
      weight: 0.000833333333333 },
    { id: 58701,
      label: 'CARAMAYOLA NIÑO VERDE',
      weight: 0.000833333333333 },
    { id: 58702,
      label: 'CARAMAYOLA NIÑO ROSADA',
      weight: 0.000833333333333 },
    { id: 58703,
      label: 'CARAMAYOLA NIÑO BLANCA',
      weight: 0.000833333333333 },
    { id: 870875, label: 'PURE LIFE S/G 6PF-PET 1500', weight: 0.01 },
    { id: 870876,
      label: 'PURE LIFE C/GAS 12PF-PET 500',
      weight: 0.006410256410256 },
    { id: 870877,
      label: 'PURE LIFE S/GAS 12PF-PET 500',
      weight: 0.006410256410256 },
    { id: 870874,
      label: 'PURE LIFE C/GAS 6PF-PET 1500',
      weight: 0.01 },
    { id: 870878,
      label: 'PURE LIFE S/GAS 4PF-BIDON 6.0',
      weight: 0.027777777777778 },
    { id: 12778,
      label: 'DISP PREMIUM BLANCO BOT AB',
      weight: 0.111111111111111 },
    { id: 999763,
      label: 'CAMBIO PRODUCTO',
      weight: 0 },
    { id: 870314,
      label: 'MANANTIAL BOT12',
      weight: 0.025 }
  ]
}

const getObservationList = () =>{
  return observationList = [
    {
      id: '421f2ab2-c0af-4534-a8e3-b266d2781e28',
      idEstado: 1,
      estado: 'Entrega completa',
      idCausa: 0,
      causa: 'Pedido Efectuado'
    },
    {
      id: '879fd9cf-e739-4367-aa36-a6414bf2c8f3',
      idEstado: 2,
      estado: 'Rechazo',
      idCausa: 9,
      causa: 'Cliente no hizo pedido'
    },
    {
      id: '62a86a90-8127-409f-a1b9-bcc7eaede05d',
      idEstado: 3,
      estado: 'Rechazo',
      idCausa: 13,
      causa: 'Cliente No Visitado Por Horario'
    },
    {
      id: 'ddaf9dfc-7d67-4171-adf7-d7da28dda0ad',
      idEstado: 4,
      estado: 'Rechazo',
      idCausa: 32,
      causa: 'Datos factura incorrectos'
    },
    {
      id: '9c8e990b-30fb-49a7-a846-03ed863582ba',
      idEstado: 5,
      estado: 'Rechazo',
      idCausa: 14,
      causa: 'Falta de Producto Stock'
    },
    {
      id: 'e0452fff-a0fa-48b4-88c8-19b964bc4945',
      idEstado: 6,
      estado: 'Rechazo',
      idCausa: 27,
      causa: 'Fuera de Ruta'
    },
    {
      id: 'db310cc9-51f4-48e5-a8e2-75cfb000b100',
      idEstado: 7,
      estado: 'Rechazo',
      idCausa: 30,
      causa: 'Local Cerrado'
    },
    {
      id: '65a2a011-7085-4dca-a8a3-051fd194f44d',
      idEstado: 8,
      estado: 'Rechazo',
      idCausa: 31,
      causa: 'Mal digitado'
    },
    {
      id: 'c606d04f-bafa-4edc-86ca-3ba59af6bd3c',
      idEstado: 9,
      estado: 'Rechazo',
      idCausa: 28,
      causa: 'Sin acceso al lugar'
    },
    {
      id: '5754bd59-3c55-49cf-bebb-faac889fcf9a',
      idEstado: 10,
      estado: 'Rechazo',
      idCausa: 6,
      causa: 'Sin dinero'
    },
    {
      id: 'e8ef032f-23d3-4bf5-9112-fd776ec7e2e7',
      idEstado: 11,
      estado: 'Rechazo',
      idCausa: 23,
      causa: 'Sin el encargado'
    },
    {
      id: '6fdf0125-501e-4b7d-8cbe-02640eb4dfb6',
      idEstado: 12,
      estado: 'Rechazo',
      idCausa: 26,
      causa: 'Sin todos los envases'
    },
    {
      id: '0f54ed46-bd18-4579-8ed4-95526c73942f',
      idEstado: 13,
      estado: 'Rechazo',
      idCausa: 490,
      causa: 'Accidente en ruta'
    },
    {
      id: 'fee2add2-a3bc-4680-b5ee-773b619b5eb5',
      idEstado: 14,
      estado: 'Rechazo',
      idCausa: 461,
      causa: 'Conflicto cliente-tripulación'
    },
    {
      id: '89984877-a017-42b9-9ee1-2c3e3757f78d',
      idEstado: 15,
      estado: 'Rechazo',
      idCausa: 49,
      causa: 'Error en Pedido'
    },
    {
      id: 'f669e823-ce82-44ca-a245-1f3f67654010',
      idEstado: 16,
      estado: 'Rechazo',
      idCausa: 130,
      causa: 'Falta espacio en bodega'
    },
    {
      id: '0d706a04-d0db-4c49-99cf-d1ac26af1170',
      idEstado: 17,
      estado: 'Rechazo',
      idCausa: 180,
      causa: 'No reconoce descuento'
    },
    {
      id: '911520d7-9c8e-4edd-83a9-b39f7029059e',
      idEstado: 18,
      estado: 'Rechazo',
      idCausa: 400,
      causa: 'Producto en mal estado'
    },
    {
      id: '27c570b4-1a3b-4150-9add-dabcb1c7884d',
      idEstado: 19,
      estado: 'Rechazo',
      idCausa: 370,
      causa: 'Producto mal cargado'
    }
  ]
}

const getAccountData = async() => {
  return accountData = [
    {
      id : 'cdt',
      name: 'Calera de Tango',
      filePath : 'MANT_CALERA_DE',
      fileName : 'PD8320.rn',
      token : 'Token fd834b0df45a5ce87974d2e058658c0bb06b95cb',
      account_id : 10673,
      account_name : 'Manantial Real'
    },
    {
      id : 'stgo',
      name: 'Santiago',
      filePath : 'MANANTIAL',
      fileName : 'PD8252.rn',
      token : 'Token e5f6ec0db8864b3cdd9c003c806c91d268f9c5b7',
      account_id : 11674,
      account_name : 'Manantial Simulaciones'
    }
  ]
}

const ItemsValidation = (itemsNotFound, msg) => {
  for (const item of itemsNotFound) {
    msg = msg.concat(' ' + item);
  }
  sendEmail(msg);
  return;
}

module.exports = {filterFilesByName, filterFiles, getProducts, getObservationList, getAccountData, ItemsValidation};