const nodemailer = require('nodemailer');

const sendEmail = async(msg) => {

  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'manantialsoporte@gmail.com',
      pass: 'manantial1234'
    }
  });

  const mailOptions = {
    from: 'manantialsoporte@gmail.com', // sender address
    // dclaver@ccu.cl
    // ddmarin@ccu.cl
    to: 'orlando.campos@simplit-solutions.com; nicola.simioni@simplit-solutions.com; vanessa.gutierrez@simplit-solutions.com',  // list of receivers
    subject: 'Error detected', // Subject line
    html: `<p>${msg}</p>`// plain text body
  };
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, (err, info) => {
      if(err){
        reject(err)
      }else{
        resolve(info);
      }
    })
  })
  .then((res) => {
    console.log(res);
    return res;
  })
  .catch((err) => {
    console.log(err);
    return err;
  })
}

module.exports = {sendEmail};
