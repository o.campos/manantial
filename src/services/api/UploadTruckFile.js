const {idVehicles, vehiclesName, getVisits, getPlan} = require("./UploadTruckFile.Service");
const {getTruckFile, sendTruckFile} = require ('./DownloadTruckFile');
const {getAccountData} = require('../../utils/utilities');

const vehicles = [];
const routes = [];
const vehicleList = [];
let visits = [];
let zone = '';


module.exports.prepareTruckFile = async(event) => {
  try {
		if (event.body) {
      whPlan = JSON.parse(event.body);
      console.log('event body plan : ', whPlan);
    }
	}
	catch(e){
		console.log(e);
		throw e;
  }

    const accounts = await getAccountData();
    const account = accounts.find(a => a.account_id === whPlan.account_id);

    const url = 'https://api.simpliroute.com/v1/routes/plans/';
    const plan = await getPlan(url, whPlan.id, account.token);
    console.log('Plan', plan);

  for(route of plan.routes){
    const url = `https://api.simpliroute.com/v1/routes/routes/${route}`;
    let p = await idVehicles(url, account.token);
    routes.push(p);
    vehicles.push(p.vehicle);
  }

  for(vehicle of vehicles){
    const url = `https://api.simpliroute.com/v1/routes/vehicles/${vehicle}`;
    let vname = await vehiclesName(url, account.token);
    vehicleList.push(vname);
  };

  setRouteOrder();

  for(route of plan.routes){
    const url = `https://api.simpliroute.com/v1/routes/visits/?route=${route}`;
    let res = await getVisits(url, account.token);
    visits = visits.concat(res);
  }

  changeIdVehicleByName();
  changeIdRouteByOrder();
  setIdParadaInterna();

  const file = await getTruckFile(account.filePath, account.fileName);
  const final = addVisitResponseToFile(file);
  console.log(final);
  let buff = new Buffer(final);
  let encodeFile = buff.toString('base64');
  console.log(encodeFile);
  sendTruckFile(encodeFile);
}

const addVisitResponseToFile = (content) => {
  let finalFile = '';
  const splittedContent = content.toString().split('\r\n');
  for (line of splittedContent) {
    let id = parseInt(line.slice(8, 23));
    let tail = getVisitResponse(id);
    let spaces = 520 - line.length;
    blank = new Array(spaces + 1).join(' ');
    finalLine = `${line}${blank}${tail}`;
    finalFile = finalFile.concat(finalLine)
  }
  return finalFile;
}

String.prototype.rjust = function(char, length, letter) {
  let fill = [];
  while ( fill.length + this.length < length ) {
    fill[fill.length] = letter;
  }
  return this + fill.join('');
}

const getVisitResponse = (id) => {
  let matchingVisit = visits.find(v => v.idVisit === id);
  let tail = ''
  if(matchingVisit !== undefined){
    matchingVisit.idCamion = matchingVisit.idCamion.toString().rjust(matchingVisit.idCamion, 10, " ");
    matchingVisit.NumRuta = matchingVisit.NumRuta.toString().rjust(matchingVisit.NumRuta, 5, " ");
    matchingVisit.NumViaje = matchingVisit.NumViaje.toString().rjust(matchingVisit.NumViaje, 5, " ");
    matchingVisit.idParadaInterna = matchingVisit.idParadaInterna.toString().rjust(matchingVisit.idParadaInterna, 10, " ");

    tail = `${matchingVisit.idCamion}${matchingVisit.NumRuta}${matchingVisit.NumViaje}${matchingVisit.idParadaInterna}\r\n`;
    return tail;
  }else{
    return ' \r\n';
  }
}

const changeIdVehicleByName = () => {
  visits.forEach(visit => {
    let vehicle = vehicleList.find(v => v.id === visit.idCamion);
    visit.idCamion = vehicle.name;
  });
}

const changeIdRouteByOrder = () => {
  visits.forEach(visit => {
    let route = routes.find(r => r.ruta === visit.NumRuta);
    visit.NumRuta = route.order;
  });
}

const setRouteOrder = () => {
  sortRoutesByTimeAndVehicle();
  let order = 1;
  let vehicle = routes[0].vehicle;
  routes.forEach(route => {
    if(route.vehicle !== vehicle){
      route.order = 1;
    }else{
      route.order = order;
      order++
    }
    vehicle = route.vehicle
  });
}

const sortRoutesByTimeAndVehicle = () => {
  routes.sort(function (a, b) {
    let aSize = a.vehicle;
    let bSize = b.vehicle;
    let aLow = a.time_start;
    let bLow = b.time_start;

    if(aSize == bSize) {
        return (aLow < bLow) ? -1 : (aLow > bLow) ? 1 : 0;
    }else{
        return (aSize < bSize) ? -1 : 1;
    }
  });
}

const sortVisit = () => {
  visits.sort(function (a, b) {
    let aSize = a.idCamion;
    let bSize = b.idCamion;
    let aLow = a.NumRuta;
    let bLow = b.NumRuta;

    if(aSize == bSize) {
        return (aLow < bLow) ? -1 : (aLow > bLow) ? 1 : 0;
    }else{
        return (aSize < bSize) ? -1 : 1;
    }
  });
}

const setIdParadaInterna = () => {
  sortVisit();
  let idCamion = visits[0].idCamion;
  let route = visits[0].NumRuta;
  let order = 0;
  visits.forEach(visit => {
    if(visit.idCamion === idCamion && visit.NumRuta === route){
      order ++;
      visit.idParadaInterna = order;
    }else if(visit.idCamion === idCamion && visit.NumRuta !== route){
      order = order + 2;
      visit.idParadaInterna = order;
    }else if(visit.idCamion !== idCamion){
      order = 1;
      visit.idParadaInterna = order;
    }
    route = visit.NumRuta;
    idCamion = visit.idCamion;
  })
}