process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
const request = require('request');
const {sendEmail} = require('../email/email');

const options =
{
  method: 'POST',
  url: 'https://api.ccu.cl/api/truck/token',
  headers: {
     Authorization: 'Basic eUpZaElFY2F5TTVnenROR1NoOUZoVmpJOEhFYTprSXhfemZwWGxCcGUxNlFuMzNVZGkzcXdqRjRh'
    },
  form: {
     grant_type: 'client_credentials'
    }
};

module.exports = async() => {
  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (error) reject(error);
      const token = JSON.parse(body, null, 4);
      resolve(token.access_token);
    })
  })
  .then((res) => {
    return res;
  })
  .catch((err) => {
    let msg = 'Error :' + err;
    let lala = endEmail(msg);
  })
};