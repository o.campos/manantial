const axios = require('axios');

const idVehicles = async(url, token) => {
  let res  =  await axios.get(url,
    {
      headers: {
        'Authorization': token
        }
    }
  )
  let data = {
    ruta: res.data.id,
    vehicle: res.data.vehicle,
    time_start: res.data.estimated_time_start,
    order: null
  }
  return(data);
};

const vehiclesName = async(url, token) => {
  let res  =  await axios.get(url,
    {
      headers: {
        'Authorization': token
      }
    }
  )
  let data = {
    id: res.data.id,
    name: res.data.name
  }
  return(data);
};

const getVisits = async(url, token) => {
  let res = await axios.get(url,
    {
      headers: {
        'Authorization': token
      }
    }
  )

  const visits = res.data.map((v) => {
    return {
      idVisit: parseInt(v.reference),  //FAIL
      idCamion: v.vehicle,
      NumRuta: v.route,
      NumViaje: v.order,
      idParadaInterna: null
    };
  });
  return visits;
};

const getPlan = async(url, planID, token) => {
  console.log('pid',planID)
  let res = await axios.get(url,
    {
      headers: {
        'Authorization': token
      }
    }
  )
  let plan = res.data.find(p => p.id === planID);
  return plan;
}

module.exports = {idVehicles, vehiclesName, getVisits, getPlan};
