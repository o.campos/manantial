const request = require("request");
const getToken = require("./Token");
const {sendEmail} = require('../email/email');

const getTruckFile = async(path, fileName) => {
  let token = await getToken();
  const options = {
    method: 'POST',
    url: 'https://api.ccu.cl/api/truck/roadnet/SimplyRoute/v1/getFileByName',
    headers:{
      Authorization: 'Bearer ' + token
    },
    body: {
      "ruta": path,
      "nombre_archivo": fileName
    },
    json: true
  };

  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      console.log(token);
      if (typeof body === 'string') {
        return reject(body)
      }
      const realfile = {
        ruta: body['ruta'],
        encodeFile : body[' encodeFile ']
      }
      // console.log(realfile.encodeFile);
      let buff = new Buffer(realfile.encodeFile, 'base64');
      let RoadnetFile = buff.toString();
      resolve(RoadnetFile);
    });
  })
  .then((res) => {
    console.log('Download Truck file completed');
    return(res);
  })
  .catch((e) => {
    console.log('Error downloading truck file: ', e);
    let msg = 'Error downloading truck file: ' + e;
    let email = sendEmail(msg);
  });
}

const sendTruckFile = async(encodeFile, path, fileName) => {
  const token = await getToken();
  const options = {
    method: 'POST',
    url: 'https://api.ccu.cl/api/truck/roadnet/SimplyRoute/v1/saveFileByName',
    headers: {
      Authorization: 'Bearer ' + token
    },
    body: {
      "ruta": path,
      "nombre_archivo": fileName,
      "contenido_archivo": encodeFile
    },
    json: true
  };

  return new Promise((resolve, reject) => {
    request(options, function (error, response, body) {
      if (body.respuesta.codigoRespuesta !== 'TRK-MAN-10001') reject(body.respuesta.descripcionRespuesta);
      resolve(body);
    });
  })
  .then(res => {
    console.log(res)
  })
  .catch((e) => {
    console.log('Error sending truck file: ', e);
    let msg = 'Error sending truck file: ' + e;
    let email = sendEmail(msg);
  })
};

module.exports = {getTruckFile, sendTruckFile};