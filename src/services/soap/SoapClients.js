const soap = require("soap");
const {sendEmail} = require('../email/email');

const url = "http://clientes.manantial.com/ws_importa/importaciones.asmx?wsdl";

const argsClientes = {
  mensaje: "Test SR",
  X_Usuario: "SMROUTE",
  X_pass: "MANASM2019"
};

module.exports = () => {
  return new Promise((resolve, reject) => {
    soap.createClient(url, function (err, client) {
      client.Envio_Clientes_SR(argsClientes, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  })
  .then((result) => {
    console.log('Clients excel downloaded');
    return result;
  })
  .catch((error) => {
    console.log('Error Clients excel: ', error);
    let msg = 'Error Clients excel: ' + error;
    let email = sendEmail(msg);
  })
};