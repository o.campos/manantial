const soap = require("soap");
const {sendEmail} = require('../email/email');

const url = "http://clientes.manantial.com/ws_importa/importaciones.asmx?wsdl";

const argsVentas = {
  mensaje: "Test SR",
  X_Usuario: "dmarin",
  X_pass: "dmarin"
};

module.exports = () => {
  return new Promise((resolve, reject) => {
    soap.createClient(url, function (err, client) {
      client.Envio_DataExcelVentas_SR(argsVentas, function (err, result) {
        if (err) reject(err);
        resolve(result.Envio_DataExcelVentas_SRResult.diffgram.NewDataSet.GRILLA);
      });
    });
  })
  .then((res) => {
    console.log('Sales excel downloaded');
    return (res);
  })
  .catch((error) => {
    console.log('Error Sales excel', error);
    let msg = 'Error Sales excel' + error;
    let email = sendEmail(msg);
  })
};