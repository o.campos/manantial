const soap = require('soap');
const axios = require('axios')
const moment = require('moment');
const connectToSOAPAndDownloadFiles = require('./SoapClients');
const getSalesFile = require('./SoapVentas');
const {getObservationList, getAccountData, ItemsValidation} = require('../../utils/utilities');

const url = 'http://clientes.manantial.com/ws_importa/importaciones.asmx?wsdl';

module.exports.checkout = async(event) => {
	let visit;
	let account;
	try {
		if (event.body) {
			console.log('eventBody',event.body);
			let eventVisit = JSON.parse(event.body);
			console.log('VISIT STATUS :', eventVisit.status);
			const accounts = await getAccountData();
			account = accounts.find(a => a.account_name === eventVisit.account);
			visit = await getObservationID(eventVisit.id, account.token);
		}
	}
	catch(e){
		console.log(e);
		throw e;
  }

	const args = await getParameters(visit);

	soap.createClient(url, function (err, client) {
		client.Cierre_pedidos_desde_UBQTI2(args[0], function (err, result) {
			if (err) console.log("error -> ", err);
			console.log('ID REFERENCE :', visit.reference + ' -> ' + 'Estado: ', args[0].X_Estado + ' Causa: ', args[0].X_Causa, JSON.stringify(result, null, 4));
		});
	});

	if(account.id === 'cdt'){
		soap.createClient(url, function (err, client) {
			client.Actualiza_GeoreferenciaCliente(args[1], function (err, result) {
				if (err) console.log("error -> ", err);
				console.log('ID REFERENCE :', visit.reference + ' -> ', JSON.stringify(result, null, 4));
			});
		});
	}
}

const getParameters = async(visit) => {
	let userNotFound = [];
	let clientNotFound = [];
  const rawSoapResponse = await connectToSOAPAndDownloadFiles();
  const salesDB = await getSalesFile();
  const observationList = await getObservationList();
	const clientsDB = rawSoapResponse.Envio_Clientes_SRResult.diffgram.NewDataSet.GRILLA;

	const clientCode = clientsDB.find(c => c.idclienteTruck === visit.reference);
	if (clientCode === undefined) {
		let msg = `Error checkout cuenta ${account.name}: usuario no encontrado en maestro cliente ->`;
		clientNotFound.push(visit.reference);
		ItemsValidation(clientNotFound, msg)
		console.log(`Error checkout cuenta ${account.name}: usuario no encontrado en maestro cliente -> `, visit.reference);
	}

	const sicaCode = salesDB.find(s => s.CLIID === visit.reference);
	if (sicaCode === undefined) {
		let msg = `Error checkout cuenta ${account.name}: usuario no encontrado en archivo de ventas ->`;
		userNotFound.push(visit.reference);
		ItemsValidation(userNotFound, msg)
		console.log(`Error checkout cuenta ${account.name}: usuario no encontrado en arhivo de ventas -> `, visit.reference);
  }

  const matchState = observationList.find(c => c.id === visit.checkout_observation);
	let checkoutTime = visit.checkout_time;
	const lat = visit.checkout_latitude;
	const long = visit.checkout_longitude;

  checkoutTime = moment(checkoutTime).format("YYYYMMDD");

  args = [
		{
			X_usuario: 'SMROUTE',
			X_pass: 'MANASM2019',
			X_idpedido: sicaCode.PEDNROREFH.toString(),
			X_Estado: parseInt(matchState.idEstado),
			X_Causa: parseInt(matchState.idCausa),
			X_fecCierre: checkoutTime.toString(),
			X_numdocto: '0',
			X_CodClie: clientCode.codclienteSica.toString()
		},
		{
			x_Usuario: 'SMROUTE',
			x_pass: 'MANASM2019',
			x_latitud: lat.toString(),
			x_longitud: long.toString(),
			x_codclieSICA: clientCode.codclienteSica.toString()
		}
	]

  return args;
}

const getObservationID = async(visitId, token) => {
  const url = `https://api.simpliroute.com/v1/routes/visits/`;
  let res = await axios.get(url,
    {
      headers: {
        'Authorization': token
      }
    }
  )
  let visit = res.data.find(v => v.id === visitId);
  return visit;
}