module.exports = {
  roots: ["<rootDir>/src"],
  testRegex: "(/__test__/.*|(\\.|/)(test|spec))\\.js?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"]
};
