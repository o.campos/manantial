const axios = require('axios');
const moment = require('moment')

const Visit = require('./src/components/Visit/Visit');
const ParseVisit = require('./src/components/ParseVisit/ParseVisit');
const {getTruckFile} = require('./src/services/api/DownloadTruckFile');
const connectToSOAPAndDownloadFiles = require('./src/services/soap/SoapClients');
const {getProducts, getAccountData, ItemsValidation} = require('./src/utils/utilities');


const contentToVisitList = (content) => {
  const splittedContent = content.toString().split('\n');
  const visits = [];
  for (let line of splittedContent) {
    const parsedVisit = new ParseVisit(line);
    if (line.length !== 0) {
      const visit = new Visit();
      visit.setIdUbicacion(parsedVisit.getIdUbicacion());
      visit.setNumeroDeOrden(parsedVisit.getNumeroDeOrden());
      visit.setSelectorOrden(parsedVisit.getSelectorOrden());
      visit.setTipoDeOrden(parsedVisit.getTipoDeOrden());
      visit.setIdDeArticulo(parsedVisit.getIdDeArticulo());
      visit.setTamanoSkuTotal1(parsedVisit.getTamanoSkuTotal1());
      visit.setTamanoSkuTotal2(parsedVisit.getTamanoSkuTotal2());
      visit.setTamanoSkuTotal3(parsedVisit.getTamanoSkuTotal3());
      visit.setOC(parsedVisit.getOC());
      visit.setFechaVenc(parsedVisit.getFechaVenc());
      visit.setFechaSesion(parsedVisit.getFechaSesion());
      visits.push(visit);
    }
  }
  return visits;
}

module.exports.manantial = async(event) => {
  const zone = event.queryStringParameters.zone;
  const rawSoapResponse = await connectToSOAPAndDownloadFiles();
  const products = getProducts();
  const clientsDB = rawSoapResponse.Envio_Clientes_SRResult.diffgram.NewDataSet.GRILLA;
  const accounts = getAccountData();
  const account = accounts.find(a => a.id === zone);
  console.log('account', account);

  const file  = await getTruckFile(account.filePath, account.fileName);
  const integrationVisits = [];
  const visits = contentToVisitList(file);

  console.log("Total de visitas registradas: " + visits.length);

  let clientsNotFound = [];
  let productNotFound = [];
  for (visit of visits) {
    let matchingClient = clientsDB.find(cl => cl.idclienteTruck === visit.idUbicacion);
    if(matchingClient === undefined){
      clientsNotFound.push(visit.idUbicacion);
      continue;
    }

    let matchingProduct = products.find(p => p.id === visit.idDeArticulo);
    if(matchingProduct === undefined){
      productNotFound.push(visit.idDeArticulo);
      continue;
    };

    let direccion = matchingClient.direccionDespacho + ', ' + matchingClient.comunadespacho;
    visit.setDireccion(direccion);
    visit.setRazonSocial(matchingClient.razonsocial);
    visit.setTelefono(matchingClient.fonocontacto);
    visit.setEmail(matchingClient.correocontacto);
    visit.setNombre(matchingClient.nombrecontacto);
    visit.setTiempoServicio(matchingClient.tiemposervicio);
    visit.setVentanaHoraria(matchingClient.horainicio, matchingClient.horafin);
    visit.setProducto(matchingProduct.label);
    visit.setGeolocation(matchingClient.latitud, matchingClient.longitud);

    integrationVisits.push(visit);
  };

  if(clientsNotFound.length){
    let msg = 'Visitas no encontrada en maestro cliente:';
    ItemsValidation(clientsNotFound, msg);
  };

  if(productNotFound.length){
    let msg = 'Producto no encontrado en maestro producto:';
    ItemsValidation(productNotFound, msg);
  };

  const apiVisits = integrationVisits.map(v => ({
    title: v.razonSocial,
    address: v.direccion,
    latitude: latlongValidation(v.latitude),
    longitude: latlongValidation(v.longitude),
    planned_date: moment(Date.now()).add(1, 'days').format("YYYY-MM-DD").toString(),
    reference: v.idUbicacion,
    window_start: v.inicioVentana !== undefined ? v.inicioVentana : null,
    window_end: v.finVentana !== undefined ? v.finVentana : null,
    duration: '00:10:00',
    contact_name: v.nombre !== null ? v.nombre : '',
    contact_phone: v.telefono !== null ? v.telefono : '',
    contact_email: v.email !== null ? emailValidation(v.email) : null,
    notes: `${v.producto} - cantidad: ${v.tamanoSkuTotal1}`,
    load: v.tamanoSkuTotal2,
    load_2: v.tamanoSkuTotal1
  })
  )

  try{
    const insertVisitResponse  = await axios({
      url: 'https://api.simpliroute.com/v1/routes/visits/',
      headers: {
        'Authorization': account.token
      },
      method: 'POST',
      data: apiVisits
    })
    console.log(JSON.stringify(insertVisitResponse.data, null, 4));
  }catch(e){
    console.log(e)
  }

};

const latlongValidation = (latlong) =>{
  let asd = isNaN(Number(latlong)) ||  Number(latlong) === 0 ? null : Number(latlong).toFixed(6);
  if (asd > 0 && asd !== null){
    asd = asd * -1;
  }
  return asd;
};

const emailValidation = (emails) => {
  emails = emails.replace(/\s/g, '').split(',');
  const isAnEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  for(let email of emails){
    if(!isAnEmail.test(email)){
      emails = emails.filter(e => e !== email);
    }
  }
  emails = emails.toString().replace(/,/g, ', ');
  return emails;
};